public class Bag {
    private int size;

    public Bag() {
        this(0);
    }

    public Bag(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
