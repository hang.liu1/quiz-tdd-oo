import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Storage {
    private Map<Ticket, Bag> bags = new HashMap<>();
    private List<Integer> slots = new ArrayList<>();
    private final String[] size = {"small", "medium", "big"};


    public Storage() {
        this(20);
    }

    public Storage(int capacity) {
        slots.add(capacity);
    }

    public Storage(List<Integer> slots){
        this.bags = new HashMap<>();
        this.slots = slots;
    }

    public Ticket save(Bag bag) {
        return save(bag, 0);
    }

    public Ticket save(Bag bag, int slotSize) {
        if (slots.get(slotSize) <= 0) throw new IllegalArgumentException("Insufficient capacity");
        if (bag.getSize() > slotSize) throw new IllegalArgumentException(String.format("Cannot save your bag: %s %s", size[bag.getSize()], size[slotSize]));

        Ticket ticket = new Ticket();
        bags.put(ticket, bag);
        slots.set(slotSize, slots.get(slotSize) - 1);
        return ticket;
    }

    public Bag retrieve(Ticket ticket) {
        if (!bags.containsKey(ticket)) throw new IllegalArgumentException("Invalid Ticket");
        Bag bag = bags.get(ticket);
        bags.remove(ticket);

        return bag;
    }
}
