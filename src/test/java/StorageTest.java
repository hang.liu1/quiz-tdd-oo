import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class StorageTest {

    private Storage createStrage() {
        return new Storage(20);
    }

    private List<Bag> createBags(int size) {
        return IntStream.range(1, size)
                .boxed()
                .map((i) -> new Bag())
                .collect(Collectors.toList());
    }

    private List<Ticket> saveBages(Storage storage, List<Bag> bags) {
        return bags.stream()
                .map(bag -> storage.save(bag))
                .collect(Collectors.toList());
    }


    @Test
    void should_get_ticket_when_save_a_bag() {
        Storage storage = new Storage();

        Ticket ticket = storage.save(new Bag());

        assertNotNull(ticket);

    }

    @Test
    void should_get_ticket_when_save_nothing() {
        Storage storage = new Storage();

        Ticket ticket = storage.save(null);

        assertNotNull(ticket);

    }


    @Test
    void should_get_the_saved_bag_when_retrieve_use_valid_ticket() {
        Storage storage = new Storage();
        Bag savedBag = new Bag();
        Bag anotherBag = new Bag();
        Ticket ticket = storage.save(savedBag);
        Ticket anotherTicket = storage.save(anotherBag);

        Bag retrievedBag = storage.retrieve(ticket);
        Bag anotherRetrievedBag = storage.retrieve(anotherTicket);

        assertSame(savedBag, retrievedBag);
        assertNotSame(savedBag, anotherRetrievedBag);

    }

    @Test
    void should_get_error_message_when_retrieve_use_invalid_ticket() {
        Storage storage = new Storage();
        Bag savedBag = new Bag();
        Ticket ticket = storage.save(savedBag);
        storage.retrieve(ticket);

        String message = assertThrows(IllegalArgumentException.class, () -> storage.retrieve(ticket)).getMessage();

        assertEquals("Invalid Ticket", message);

    }

    @Test
    void should_get_nothing_when_retrieve_use_ticket_no_save_bag() {
        Storage storage = new Storage();
        Ticket ticket = storage.save(null);

        Bag retrievedBag = storage.retrieve(ticket);

        assertNull(retrievedBag);

    }


    @Test
    void should_get_ticket_when_save_bag_given_storage_have_empty_capacity() {
        int capacity = 2;
        Storage storage = new Storage(capacity);
        Bag bag = new Bag();

        Ticket ticket = storage.save(bag);

        assertNotNull(ticket);

    }


    @Test
    void should_get_a_ticket_and_a_message_when_save_2_bags_given_1_capacity() {
        int capacity = 1;
        Storage storage = new Storage(1);
        Bag bag1 = new Bag();
        Bag bag2 = new Bag();

        Ticket ticket = storage.save(bag1);
        String message = assertThrows(IllegalArgumentException.class, () -> storage.save(bag2)).getMessage();


        assertNotNull(ticket);
        assertEquals("Not Empty", message);

    }


    @Test
    void should_not_save_when_storage_full() {
        int capacity = 2;
        Storage storage = new Storage(2);
        saveBages(storage, createBags(capacity));

        String message = assertThrows(IllegalArgumentException.class, () -> storage.save(new Bag())).getMessage();

        assertEquals("Not Empty", message);

    }


    @Test
    void should_get_ticket_when_save_small_bag_to_big_slot() {
        Storage storage = new Storage(Arrays.asList(0, 0, 1));
        Bag bag = new Bag(0);

        Ticket ticket = storage.save(bag, 2);
        assertNotNull(ticket);
    }


    @Test
    void should_not_save_when_save_bag_bag_to_small_slot() {
        Storage storage = new Storage(Arrays.asList(1, 1, 1));
        Bag bag = new Bag(2);

        assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(bag, 0),
                "Cannot save your bag: big small");
    }

    @Test
    void should_not_save_when_save_medium_bag_to_full_big_slot() {
        Storage storage = new Storage(Arrays.asList(1, 1, 0));
        Bag bag = new Bag(1);

        assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(bag, 2),
                "Insufficient capacity");
    }

    @Test
    void should_get_ticket_when_save_medium_bag_to_medium_slot() {
        Storage storage = new Storage(Arrays.asList(0, 1, 0));
        Bag bag = new Bag(1);


        Ticket ticket = storage.save(bag, 1);
        assertNotNull(ticket);
    }
}
